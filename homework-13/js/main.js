const buyButtons=document.getElementsByClassName("btn-buy");
const contactUs=document.getElementsByClassName("btn-contact")[0];
const dark=document.getElementsByClassName("background-color-dark");
const light=document.getElementsByClassName("background-color-light");
const table=document.getElementsByTagName("table")[0];
const colorChange=document.getElementsByClassName("color-changer")[0];

let colorChanged=false;

colorChange.addEventListener("click",(event)=>{
    if(colorChanged){
        table.style.cssText="color: #484848; background-color: #fafafa;";
        contactUs.style.backgroundColor="#dddddd";
        for (let item of buyButtons)
        {
            item.style.backgroundColor="#eeeeee";
        }
        for (let item of dark)
        {

            item.style.backgroundColor="#e3e3e3";

        }
        for (let item of light)
        {
            item.style.backgroundColor="#f2f2f2";
        }
        colorChanged=false;
    }
    else{
        table.style.cssText="color: green; background-color: black;";
        contactUs.style.backgroundColor="black";
        for (let item of buyButtons)
        {
            item.style.backgroundColor="black";
        }
        for (let item of dark)
        {

            item.style.backgroundColor="black";

        }
        for (let item of light)
        {
            item.style.backgroundColor="black";
        }
        colorChanged=true;
    }
})