const akali=document.getElementsByClassName("akali")[0];
const anivia=document.getElementsByClassName("anivia")[0];
const draven=document.getElementsByClassName("draven")[0];
const garen=document.getElementsByClassName("garen")[0];
const katarina=document.getElementsByClassName("katarina")[0];

const tabs=[akali,anivia,draven,garen,katarina];

const lists=document.getElementsByClassName("text");
cleaner();
tabs[0].classList.add("active");
lists[0].hidden=false;
function cleaner(){
    for(let item of lists)
    {
        if(item.classList.contains("text")){
            item.hidden=true;
        }
    }
    for(let item of tabs)
    {
        if(item.classList.contains("active"))
        {
            item.classList.remove("active")
        }
    }
}
function showText(event,className)
{
    if (!event.target.classList.contains('active')) {
        const element = document.getElementsByClassName(className)[0];
        cleaner();
        element.hidden=false;
        event.target.classList.add("active");
    }
}

akali.addEventListener('click',(event) => {
    showText(event,"akali-text")});
anivia.addEventListener('click',(event) => {
    showText(event,"anivia-text")});
draven.addEventListener('click',(event) => {
    showText(event,"draven-text")});
garen.addEventListener('click',(event) => {
    showText(event,"garen-text")});
katarina.addEventListener('click',(event) => {
    showText(event,"katarina-text")});