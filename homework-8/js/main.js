const content= document.getElementsByClassName("content")[0];
const priceInput=document.getElementsByClassName("price-input")[0];
const contentSpan=document.getElementsByClassName("content-span")[0];
const clearBtn= document.getElementsByClassName("clear")[0];
const invalidInput=document.getElementsByClassName("invalid")[0];
contentSpan.style.cssText="display: none;";

document.body.style.cssText="background-color:orange;";
content.style.cssText = "display: flex; align-items: start; height:200px;";
clearBtn.innerText="Clear";
clearBtn.style.cssText="display: none;";
invalidInput.style.cssText="display: none";

priceInput.addEventListener("focusin",(event)=>{
    event.target.style.cssText="border: 2px solid green;"
});

priceInput.addEventListener("focusout",(event)=>{
    if(!isNaN(event.target.value) && event.target.value>=0){
        event.target.style.cssText="border: none;  color: green;";
        contentSpan.style.cssText="display: inline-block;";
        invalidInput.style.cssText="display: none";
        contentSpan.textContent=`Current price: ${event.target.value} $`;
        clearBtn.style.cssText="display: inline-block;" +
            "height: 20px;" +
            "width: 50px;" +
            "background-color: blue;" +
            "color: white;" +
            "margin-left: 10px;" ;
    }
    else{
        contentSpan.style.cssText="display: none;";
        clearBtn.style.cssText="display: none;";
        event.target.style.cssText="border: 2px solid red;  color: red;";
        invalidInput.style.cssText="display: inline-block; color: red;";
    }

})

clearBtn.addEventListener("click",(event)=>{
    event.target.style.cssText="display: none;";
    contentSpan.style.cssText="display: none;";
    priceInput.value=0;
})