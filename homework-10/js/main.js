const icons=document.getElementsByClassName("icon-password");
const confirm=document.getElementsByClassName("btn")[0];
const inputBoxFirst=document.getElementsByClassName("input-box")[0];
const inputBoxSecond=document.getElementsByClassName("input-box")[1];
const errorMsg=document.getElementsByClassName("error-msg")[0];

errorMsg.style.cssText= "display: none;";
icons.previousElementSibling
icons[0].classList.remove("fa-eye");
icons[0].classList.add("fa-eye-slash");
for(let item of icons){

    item.addEventListener("click",(event)=>{

        if(event.target.classList.contains("fa-eye-slash")){
            event.target.classList.remove("fa-eye-slash");
            event.target.classList.add("fa-eye");
            event.target.previousElementSibling.type="text";
        }
        else{
            event.target.classList.remove("fa-eye");
            event.target.classList.add("fa-eye-slash");
            event.target.previousElementSibling.type="password";
        }
    })
}

confirm.addEventListener("click",(event)=>{
    if(inputBoxFirst.value===inputBoxSecond.value){
        errorMsg.style.cssText= "display: none;";
        alert("You are welcome");
    }
    else{
        errorMsg.style.cssText="display: inline-block; color: red;";
    }
})
