const images=[
    "img/jpg1.jpg",
    "img/jpg2.jpg",
    "img/jpg3.jpg",
    "img/png1.png"
];
x=0;
const container=document.querySelector('.image-to-show');
const stop=document.querySelector('.stop-btn');

const resume=document.querySelector('.resume-btn');
function nextImage() {
    x++;
    if(x===4)
    {
        x=0;
    }
    container.src=images[x];
    console.log(x);
}

container.src=images[x];

let timer=setInterval(nextImage, 10000);

stop.addEventListener('click',()=>{
    clearInterval(timer);
});
resume.addEventListener('click',()=>{
    timer=setInterval(nextImage, 10000);
});